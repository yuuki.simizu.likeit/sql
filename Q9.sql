select category_name,item_price,
sum(item.item_price)as total_price
from item 
inner join item_category 
on item_category.category_id = item.category_id
group by item_category.category_id
order by total_price desc;